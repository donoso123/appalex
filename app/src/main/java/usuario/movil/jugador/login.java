package usuario.movil.jugador;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class login extends Activity {
    private EditText Name;
    private EditText Password;

    private Button Login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Name = (EditText)findViewById(R.id.TXTUser);
        Password = (EditText)findViewById(R.id.TXTPass);
        Login = (Button)findViewById(R.id.BTNIniciarSesion);

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate(Name.getText().toString(), Password.getText().toString());
            }
        });
    }

    private void validate(String userName, String userPassword){
        if((userName.equals("admin")) && (userPassword.equals("12345678"))){
            Intent intent = new Intent(login.this, principal.class);
            startActivity(intent);
        }else{
            Toast.makeText(login.this, "Los datos ingresados son incorrectos",Toast.LENGTH_SHORT).show();;

        }

    }
    @Override
    public void onBackPressed (){
        moveTaskToBack(true);
    }
}
