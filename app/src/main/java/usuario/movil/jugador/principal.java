package usuario.movil.jugador;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class principal extends Activity {
    Button agregarrutas;
    Button btn_lista, salir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);


        agregarrutas= (Button)findViewById(R.id.btn_agregarjugador);
        agregarrutas.setOnClickListener(new View.OnClickListener() {
            @Override


            public void onClick(View v) {
                Intent agregarrutas = new Intent(principal.this, MainActivity.class);
                startActivity(agregarrutas);

            }
        });
        btn_lista= (Button)findViewById(R.id.btn_lista);
        btn_lista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lista = new Intent(principal.this, Resultado.class);
                startActivity(lista);

            }
        });

        salir= (Button)findViewById(R.id.salir);
        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lista = new Intent(principal.this, login.class);
                startActivity(lista);

            }
        });


    }
    @Override
    public void onBackPressed (){
        moveTaskToBack(true);
    }

}