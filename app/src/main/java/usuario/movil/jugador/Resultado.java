package usuario.movil.jugador;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class Resultado extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        final HelperDB helperbd = new HelperDB(getApplicationContext());

        final TextView tvdatos = (TextView) findViewById(R.id.TV_DatosMostrar);
        tvdatos.setText(helperbd.LeerTodo());

    }
}
